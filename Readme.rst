aacgain
=========

AACGain original work by: http://aacgain.altosdesign.com/

This repository contains AACGain version 1.9.0 and is a fork of bazaar branches made by `Robert Tari`_ 

.. _`Robert Tari`: https://code.launchpad.net/~robert-tari/aacgain/trunk


How to build
=============

You should build 2 projects which are a in subfolders:

* mp4v2
* faad2


mp4v2
------

Just go in the subdir (cd mp4v2) and:

::

  ./configure 
  make

faad2
------

Same as mp4v2

::

  cd faad2
  ./configure && make

aacgain
--------

Now you can run configure and make on the top level, not in aacgain subfolder. 


All-in-one
----------


You are lazy:

::

  cd mp4v2
  ./configure && make
  cd ../faad2
  ./configure && make
  cd ..
  ./configure && make

Support
========

There is few chances I will provide support, I mainly make a clone of the sources to install it on Ubuntu Trusty (14.04).
